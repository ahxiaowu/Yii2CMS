<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => '后台管理',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'defaultRoute' => 'site/index',
    'aliases' => [
        '@mdm/admin' => dirname(dirname(dirname(__FILE__))).'\vendor\mdmsoft\yii2-admin',
    ],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\User',
                    'idField' => 'id',
                ]
            ],
            'menus' => [
            'assignment' => [
                'label' => 'Grand Access' // change label
                ],
            //'route' => null, // disable menu route
            ],
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'request' => [
            'cookieValidationKey' => 'HelloSZY',
            'csrfCookie' => [
                'httpOnly' => true,
                'path' => '/admin',
            ],
        ],
        'user' => [
            'identityCookie' => [
                'name' => 'HelloSZY2',
                'path' => '/admin',
                'httpOnly' => true,
            ],
        ],
        'session' => [
            'name' => 'BACKENDSESSID',
            'cookieParams' => [
                'path' => '/admin',
            ],
        ],
        'authManager' => [
            'class' => 'mdm\admin\components\DbManager',//认证类名称
            //'class' => 'yii\rbac\DbManager',
            'itemTable' => '{{%auth_item}}',//认证项表名称
            'itemChildTable' => '{{%auth_item_child}}',//认证父子关系
            'assignmentTable' => '{{%auth_assignment}}',//认证项赋权关系
            'ruleTable' => '{{%auth_rule}}',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@mdm/admin/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'zh',
                    'fileMap' => [
                        //'main' => 'main.php',
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'api-yd/*','site/tv','interface/*'//免登陆
        ],
    ],
    'params' => $params,
];
