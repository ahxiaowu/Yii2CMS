<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OmsArticle */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Oms Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oms-article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('更新', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '你确定你要删除这篇文章吗?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            //'content:ntext',
            'add_time',
            'author',
            'type_id',
            'status',
        ],
    ]) ?>
    
    <table class="table table-striped table-bordered detail-view">
    	<tbody>
    		<tr>
    			<th rolspan=2>内容</th>
			</tr>
			<tr>
    			<td rolspan=2><?= htmlspecialchars_decode($model->content) ?></td>
			</tr>
		</tbody>
	</table>

</div>
