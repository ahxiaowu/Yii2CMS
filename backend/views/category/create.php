<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SysCategory */

$this->title = '添加';
$this->params['breadcrumbs'][] = ['label' => 'Sys Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sys-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
