<?php

/**
 * 获取给定日期的前一天
 * @param string $date
 * @return string $yesterday
 * @author: zeyong.shi
 * @date: 2015年5月27日下午1:56:56
 * @modified_date: 2015年5月27日下午1:56:56
 * @modified_user: zeyong.shi
 */
function getYesterday($date) {
    if (empty($date)) {
        $yesterday = date("Y-m-d", strtotime("-1 day"));
    } else {
        $arr = explode('-', $date);
        $year = $arr[0];
        $month = $arr[1];
        $day = $arr[2];
        $unixtime = mktime(0, 0, 0, $month, $day, $year) - 86400;
        $yesterday = date('Y-m-d', $unixtime);
    }
    return $yesterday;
}

/**
 * 上传图片
 * 将图片地址保存到数据中
 * @author: zeyong.shi
 * @date: 2015年6月10日下午3:05:07
 * @modified_date: 2015年6月10日下午3:05:07
 * @modified_user: zeyong.shi
 */
function uploadImage($arr) {
    $_FILES = $arr;
    $uptypes = array(
        'image/jpg',
        'image/jpeg',
        'image/png',
        'image/pjpeg',
        'image/gif',
        'image/bmp',
        'image/x-png'
    );

    $max_file_size = 2000000;     //上传文件大小限制, 单位BYTE
    $destination_folder = "../../upload/"; //上传文件路径
    $watermark = 2;      //是否附加水印(1为加水印,其他为不加水印);
    $watertype = 1;      //水印类型(1为文字,2为图片)
    $waterposition = 1;     //水印位置(1为左下角,2为右下角,3为左上角,4为右上角,5为居中);
    $waterstring = "";  //水印字符串
    $waterimg = "xplore.gif";    //水印图片
    $imgpreview = 1;      //是否生成预览图(1为生成,其他为不生成);
    $imgpreviewsize = 1 / 2;    //缩略图比例

    if (!is_uploaded_file($_FILES["upfile"]['tmp_name'])) {//是否存在文件
        echo "图片不存在!";
        exit;
    }

    $file = $_FILES["upfile"];
    if ($max_file_size < $file["size"]) {//检查文件大小
        echo "文件太大!";
        exit;
    }

    if (!in_array($file["type"], $uptypes)) {//检查文件类型
        echo "文件类型不符!" . $file["type"];
        exit;
    }

    if (!file_exists($destination_folder)) {
        mkdir($destination_folder);
    }

    $filename = $file["tmp_name"];
    $image_size = getimagesize($filename);
    $pinfo = pathinfo($file["name"]);
    $ftype = $pinfo['extension'];
    $destination = $destination_folder . time() . "." . $ftype;
    if (file_exists($destination) && $overwrite != true) {
        echo "同名文件已经存在了";
        exit;
    }

    if (!move_uploaded_file($filename, $destination)) {
        echo "移动文件出错";
        exit;
    }

    $pinfo = pathinfo($destination);
    $fname = $pinfo['basename'];
    $mes = " 宽度:" . $image_size[0];
    $mes .= " 长度:" . $image_size[1];
    $mes .= "<br> 大小:" . $file["size"] . " bytes";

    if ($watermark == 1) {
        $iinfo = getimagesize($destination, $iinfo);
        $nimage = imagecreatetruecolor($image_size[0], $image_size[1]);
        $white = imagecolorallocate($nimage, 255, 255, 255);
        $black = imagecolorallocate($nimage, 0, 0, 0);
        $red = imagecolorallocate($nimage, 255, 0, 0);
        imagefill($nimage, 0, 0, $white);
        switch ($iinfo[2]) {
            case 1:
                $simage = imagecreatefromgif($destination);
                break;
            case 2:
                $simage = imagecreatefromjpeg($destination);
                break;
            case 3:
                $simage = imagecreatefrompng($destination);
                break;
            case 6:
                $simage = imagecreatefromwbmp($destination);
                break;
            default:
                die("不支持的文件类型");
                exit;
        }

        imagecopy($nimage, $simage, 0, 0, 0, 0, $image_size[0], $image_size[1]);
        imagefilledrectangle($nimage, 1, $image_size[1] - 15, 80, $image_size[1], $white);

        switch ($watertype) {
            case 1:   //加水印字符串
                imagestring($nimage, 2, 3, $image_size[1] - 15, $waterstring, $black);
                break;
            case 2:   //加水印图片
                $simage1 = imagecreatefromgif("xplore.gif");
                imagecopy($nimage, $simage1, 0, 0, 0, 0, 85, 15);
                imagedestroy($simage1);
                break;
        }

        switch ($iinfo[2]) {
            case 1:
                //imagegif($nimage, $destination);
                imagejpeg($nimage, $destination);
                break;
            case 2:
                imagejpeg($nimage, $destination);
                break;
            case 3:
                imagepng($nimage, $destination);
                break;
            case 6:
                imagewbmp($nimage, $destination);
                //imagejpeg($nimage, $destination);
                break;
        }

        //覆盖原上传文件
        imagedestroy($nimage);
        imagedestroy($simage);
    }

    $img_url = false;
    if ($imgpreview == 1) {
        $img_url = f_remote_upload($destination);
    }
    return $img_url;
}

/**
 * 二维数组排序
 * @param array $multi_array 需要排序的数组
 * @param string $sort_key 以该键进行排序
 * @param string $sort 排序方式 SORT_ASC/SORT_DESC
 * @author: user
 * @date: 2015年8月4日上午11:30:18
 * @modified_date: 2015年8月4日上午11:30:18
 * @modified_user: user
 */
function multi_array_sort($multi_array, $sort_key, $sort = SORT_ASC) {
    if (is_array($multi_array)) {
        foreach ($multi_array as $row_array) {
            if (is_array($row_array)) {
                $key_array[] = $row_array[$sort_key];
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
    array_multisort($key_array, $sort, $multi_array);
    return $multi_array;
}

/**
 *
 * 字符串截取函数，防止截取中文乱码
 * @param String $sourcestr 截取的字符串对象
 * @param Integer $cutlength 截取的长度
 * @param String $etc 截取完的后缀，默认为'...'
 */
function t_sub($sourcestr, $cutlength = 80, $etc = '...') {
    $returnstr = '';
    $i = 0;
    $n = 0.0;
    $str_length = strlen($sourcestr);
    while (($n < $cutlength) and ( $i < $str_length)) {
        $temp_str = substr($sourcestr, $i, 1);
        $ascnum = ord($temp_str);
        if ($ascnum >= 252) {
            $returnstr = $returnstr . substr($sourcestr, $i, 6);
            $i = $i + 6;
            $n++;
        } elseif ($ascnum >= 248) {
            $returnstr = $returnstr . substr($sourcestr, $i, 5);
            $i = $i + 5;
            $n++;
        } elseif ($ascnum >= 240) {
            $returnstr = $returnstr . substr($sourcestr, $i, 4);
            $i = $i + 4;
            $n++;
        } elseif ($ascnum >= 224) {
            $returnstr = $returnstr . substr($sourcestr, $i, 3);
            $i = $i + 3;
            $n++;
        } elseif ($ascnum >= 192) {
            $returnstr = $returnstr . substr($sourcestr, $i, 2);
            $i = $i + 2;
            $n++;
        } elseif ($ascnum >= 65 and $ascnum <= 90 and $ascnum != 73) {
            $returnstr = $returnstr . substr($sourcestr, $i, 1);
            $i = $i + 1;
            $n++;
        } elseif (!(array_search($ascnum, array(37, 38, 64, 109, 119)) === FALSE)) {
            $returnstr = $returnstr . substr($sourcestr, $i, 1);
            $i = $i + 1;
            $n++;
        } else {
            $returnstr = $returnstr . substr($sourcestr, $i, 1);
            $i = $i + 1;
            $n = $n + 0.5;
        }
    }

    if ($i < $str_length) {
        $returnstr = $returnstr . $etc;
    }

    return $returnstr;
}

/**
 * 根据用户ID返回用户相关信息
 * @param int $user_id
 * @author: user
 * @date: 2015年8月5日下午1:56:04
 * @modified_date: 2015年8月5日下午1:56:04
 * @modified_user: user
 */
function t_user($user_id) {
    $q = new \yii\db\Query();
    return $q->select('*')->from('dh_sys_admin_user')->where(['id' => $user_id])->one();
}
