<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%oms_comments}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $article_id
 * @property string $name
 * @property string $email
 * @property string $content
 * @property string $url
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%oms_comments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'article_id'], 'integer'],
            [['article_id', 'name', 'email', 'content', 'add_time'], 'required'],
            [['add_time'], 'safe'],
            [['name'], 'string', 'max' => 45],
            [['email', 'content', 'url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => '父类ID',
            'article_id' => '文章ID',
            'name' => '昵称',
            'email' => 'Email',
            'content' => '评论内容',
            'url' => 'Url',
            'add_time' => '发布时间',
        ];
    }
}
