<?php
return [
    'adminEmail' => 'szy361@126.com',
    'supportEmail' => 'szy361@126.com',
    'user.passwordResetTokenExpire' => 3600,
    'mdm.admin.configs' => [
        'db' => 'db',
        'menuTable' => '{{%menu}}',
        'cache' => null,
        'cacheDuration' => 3600
    ],
];
