<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'zh-CN',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=yc',
            'username' => 'root',
            'password' => 'szy19900316',
            'charset' => 'utf8',
            'tablePrefix' => 'yc_',
            'enableSchemaCache' => false,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'test' => 'site/test',
                'log/model'=>'log/model-log',
                'log/sys' => 'log/sys-log',
                'log/user' => 'log/user-log',
                'cache' => 'sys-cache/init-cache',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
    ],
];
