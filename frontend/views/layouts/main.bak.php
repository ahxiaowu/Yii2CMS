<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Just for fun',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => '首页', 'url' => ['/site/index']],
                ['label' => '发现', 'url' => ['/site/about']],
                ['label' => '关于我们', 'url' => ['/site/about']],
                ['label' => '联系我们', 'url' => ['/site/contact']],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => '注册', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => '登录', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => '退出 (' . Yii::$app->user->identity->login_account . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);

            echo Html::beginForm(['site/search'], 'get', ['class' => 'navbar-form navbar-right']);
            echo Html::textInput('search','',['class' => 'form-control','placeholder' => '全站搜索']);
            echo Html::endForm();
            
            NavBar::end();
        ?>

        <div class="container">
            <?= Alert::widget() ?>
            <div class="row" style="border:1px solid #E6E6E6;margin-top:15px;border-radius:5px;">
                <div class="col-md-9" style="border-right:1px solid #E6E6E6;padding: 0;">
                    <div style="border-bottom:1px solid #E6E6E6;width:100%;height:50px;">
                        <span class="glyphicon glyphicon-th-list" style="font-size: 25px;  margin-top: 10px;margin-left: 15px;margin-right: 10px;"></span>
                        <span style="font-size: 25px;">发现</span>
                    </div>
                    <div style="padding-left: 15px;padding-right:15px;">
                        <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                        <?= $content ?>
                    </div>
                </div>
                <div class="col-md-3" style="padding: 0;">
                    <div style="width: 100%;">
                        <div style="padding-left: 15px;padding-top: 10px;width: 100%;height:50px;font-size: 20px;border-bottom: 1px solid #E6E6E6;">最新评论</div>
                        <div style="padding-left:10px;padding-top:10px;padding-right:10px;">
                            <p>123</p>
                            <p>123</p>
                            <p>123</p>
                            <p>123</p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <footer class="footer" style="background: black;color:#fff;">
        <div class="container">
        <p class="pull-left">&copy; Qmsu <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
