<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php require 'header.php'; ?>
<div class="col-md-8">
    <div class="blog-item">
        <!-- 显示文章图片 -->
        <img class="img-responsive img-blog" style="display: none;" src="/images/blog/blog1.jpg" width="100%" alt="" />
        <div class="row">  
            <div class="col-xs-12 col-sm-2 text-center">
                <div class="entry-meta">
                    <span id="publish_date"><?= date("Y-m-d", strtotime($article['add_time'])) ?></span>
                    <span><i class="fa fa-user"></i> <a href="#"> <?= $article['author'] ?></a></span>
                    <span><i class="fa fa-comment"></i> <a href="blog-item.html#comments">2 评论</a></span>
                    <span><i class="fa fa-heart"></i><a href="#">56 赞</a></span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-10 blog-content">
                <h2><?= $article['title'] ?></h2>
                <p><?= $article['content'] ?></p>
                <!-- 显示标签 -->
                <div class="post-tags" style="display: none;">
                    <strong>Tag:</strong> <a href="#">Cool</a> / <a href="#">Creative</a> / <a href="#">Dubttstep</a>
                </div>

            </div>
        </div>
    </div><!--/.blog-item-->

    <div class="media reply_section">
        <div class="pull-left post_reply text-center">
            <a href="#"><img src="/images/blog/boy.png" class="img-circle" alt="" /></a>
            <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i> </a></li>
            </ul>
        </div>
        <div class="media-body post_reply_content">
            <h3>Taoke</h3>
            <p class="lead">PHPer,Linuxer,MySQL,javaer</p>
            <p class="lead">本博客文章可随意转载，转载请注明出处</p>
            <p><strong>Web:</strong> <a href="http://www.szy361.com">www.szy361.com</a></p>
        </div>
    </div> 

    <?php if (!empty($comments)) { ?>
        <h1 id="comments_title"><?= count($comments) ?> 评论</h1>
    <?php foreach ($comments as $v) { ?>
            <div class="media comment_section">
                <div class="pull-left post_comments">
                    <a href="#"><img src="/images/blog/boy2.png" class="img-circle" alt="" /></a>
                </div>
                <div class="media-body post_reply_comments">
                    <h3><?= $v['name'] ?></h3>
                    <h4><?= $v['add_time'] ?></h4>
                    <p><?= $v['content'] ?></p>
                    <a href="#">回复</a>
                </div>
            </div> 
        <?php }
    }
    ?>

    <div id="contact-page clearfix">
        <div class="status alert alert-success" style="display: none"></div>
        <div class="message_heading">
            <h4>评论</h4>
            <p>注：* 是必填项.不支持HTML代码！</p>
        </div> 

<?php //$form = ActiveForm::begin();  ?>
        <form id="w0" method="post" action="/comments/create">
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>昵称 *</label>
                        <input type="text" maxlength="45" name="Comments[name]" class="form-control" id="comments-name" required="required">
                    </div>
                    <div class="form-group">
                        <label>邮箱 *</label>
                        <input type="text" maxlength="255" name="Comments[email]" class="form-control" id="comments-email" required="required">
                    </div>
                    <div class="form-group">
                        <label>你的网址</label>
                        <input type="text" maxlength="255" name="Comments[url]" class="form-control" id="comments-url">
                    </div>                    
                </div>
                <div class="col-sm-7">                        
                    <div class="form-group">
                        <label>内容 *</label>
                        <textarea name="Comments[content]" id="comments-content" required="required" class="form-control" rows="8"></textarea>
                    </div>                        
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg" required="required">就这么任性的留下一脚</button>
                    </div>
                </div>
            </div>
            <input type="hidden" value="YnZYRU5mblMzDG8OCiwXH1cfMAEbAx4hLDgvJHkjBSs4Ew0EOSQjGw==" name="_csrf">
            <input type="hidden" name="Comments[add_time]" class="form-control" id="comments-add_time" value="<?= date('Y-m-d H:i:s') ?>">
            <input type="hidden" name="Comments[article_id]" class="form-control" id="comments-article_id" value="<?= $article['id'] ?>">
        </form>
<?php //ActiveForm::end();  ?>
    </div><!--/#contact-page-->
</div><!--/.col-md-8-->
<?php require 'footer.php'; ?>